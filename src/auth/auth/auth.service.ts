import { Injectable } from '@nestjs/common';
import { UserService } from '../user/user.service';
import { JwtService } from '@nestjs/jwt';
import { User } from '../user.entity';

@Injectable()
export class AuthService {
  constructor(
    private readonly userService: UserService,
    private readonly jwtService: JwtService,
  ) {}
  private async validate(userData: User): Promise<User> {
    return await this.userService.findByEmail(userData.email);
  }
  public async login(user: User): Promise<any | { status: number }> {
    console.log(user);
    return this.validate(user).then((userData) => {
      console.log(userData);
      if (!userData) {
        return {
          status: 404,
          message: 'Email không tồn tại',
        };
      }
      if (userData.password !== user.password) {
        return {
          status: 404,
          message: 'Mật khẩu không chính xác',
        };
      }
      const payload = `${userData.name}${userData.id}`;
      const accessToken = this.jwtService.sign(payload);
      return {
        exprise_in: 3600,
        access_token: accessToken,
        user_id: payload,
        status: 200,
      };
    });
  }
  public async register(user: User): Promise<any> {
    return this.userService.create(user);
  }
}
