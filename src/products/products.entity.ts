import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity('products')
export class ProductsEntity {
  @PrimaryGeneratedColumn()
  id: number;
  @Column()
  name: string;
  @Column()
  quantity: number;
  @Column()
  isActive: number;
  @Column({ type: 'date' })
  createdAt: Date;
  @Column({ type: 'date' })
  updatedAt: Date;
  @Column()
  company: string;
}
