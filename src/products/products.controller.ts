import {
  Body,
  Controller,
  Delete,
  Get,
  HttpStatus,
  Param,
  Patch,
  Post,
} from '@nestjs/common';
import { ProductsService } from './products.service';
import { ProductsDTO } from './products.dto';

@Controller('products')
export class ProductsController {
  constructor(private productsService: ProductsService) {}
  @Get()
  async showAllProduct() {
    return {
      statusCode: HttpStatus.OK,
      data: await this.productsService.showAll(),
    };
  }
  @Post()
  async createProduct(@Body() data: ProductsDTO) {
    return {
      statusCode: HttpStatus.OK,
      message: 'Thêm thành công',
      data: await this.productsService.create(data),
    };
  }
  @Get(':id')
  async readUser(@Param('id') id: number) {
    if ((await this.productsService.read(id)) !== undefined) {
      return {
        statusCode: HttpStatus.OK,
        data: await this.productsService.read(id),
      };
    } else {
      return {
        statusCode: HttpStatus.NOT_FOUND,
        data: null,
      };
    }
  }
  @Patch(':id')
  async uppdateUser(
    @Param('id') id: number,
    @Body() data: Partial<ProductsDTO>,
  ) {
    return {
      statusCode: HttpStatus.OK,
      message: 'Product update successfully',
      data: await this.productsService.update(id, data),
    };
  }

  @Delete(':id')
  async deleteUser(@Param('id') id: number) {
    await this.productsService.destroy(id);
    return {
      statusCode: HttpStatus.OK,
      message: 'Product deleted successfully',
    };
  }
}
