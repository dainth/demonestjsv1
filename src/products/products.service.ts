import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { ProductsEntity } from './products.entity';
import { ProductsDTO } from './products.dto';

@Injectable()
export class ProductsService {
  constructor(
    @InjectRepository(ProductsEntity)
    private productRepository: Repository<ProductsEntity>,
  ) {}
  async showAll() {
    return await this.productRepository.find();
  }
  async create(data: ProductsDTO) {
    const product = this.productRepository.create(data);
    await this.productRepository.save(data);
    return product;
  }
  async read(id: number) {
    return await this.productRepository.findOne({
      where: {
        id: id,
      },
    });
  }
  async update(id: number, data: Partial<ProductsDTO>) {
    await this.productRepository.update({ id }, data);
    return await this.productRepository.findOne({ id });
  }
  async destroy(id: number) {
    await this.productRepository.delete({ id });
    return { deleted: true };
  }
}
