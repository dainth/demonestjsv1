export interface ProductsDTO {
  id: number;
  name: string;
  quantity: number;
  createdAt: Date;
  updatedAt: Date;
  isActive: number;
  company: string;
}
