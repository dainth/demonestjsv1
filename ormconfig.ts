import { MYSQL} from "./config";

export = {
  host: MYSQL.HOST,
  type: 'mysql',
  port: MYSQL.PORT,
  username: MYSQL.USERNAME,
  password: MYSQL.PASSWORD,
  database: MYSQL.DATABASE,
  migrations: [
    'src/database/migrations/*.ts',
  ],
  cli: {
    migrationsDir: 'src/database/migrations',
  },
};
